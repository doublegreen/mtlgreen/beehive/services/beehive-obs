# Beehive OBS

[OBS Studio](https://obsproject.com/) setup and configuration to host the Beehive Live Streaming Show.

## License & Attribution

The Beehive OBS Studio configuration and assets are made available under the Creative Commons CC0 1.0 License, meaning you are free to use it for any purpose, commercial or non-commercial, without any attribution back to the original authors (public domain).
